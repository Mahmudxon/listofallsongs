package uz.mahmudxon.listofallsongs;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import uz.mahmudxon.listofallsongs.Adapters.CustomAdapter;
import uz.mahmudxon.listofallsongs.Models.ItemData;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<ItemData> data;
    CustomAdapter adapter;
    MediaPlayer player;
    private static final int MY_PERMISSION_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = findViewById(R.id.list);
        loadData();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String url = data.get(position).
                        getUrl();
                if(player == null)
                player = MediaPlayer.create(MainActivity.this, Uri.parse(url));
                else
                {
                    player.stop();
                    player = null;
                    player = MediaPlayer.create(MainActivity.this, Uri.parse(url));
                }

                player.start();
            }
        });
    }

    private void loadData() {
        data = new ArrayList<>();
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            if(ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }
            else
            {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }
        }
        else
        {
           getMusic();
            
        }
    }

    private void getMusic()
    {   
        ContentResolver contentResolver = getContentResolver();
        Uri songUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Cursor cursor = contentResolver.query(songUri, null, null, null, null);
        if(cursor != null && cursor.moveToFirst())
        {
            int songTitle = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            int songArtist = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            int songUrl = cursor.getColumnIndex(MediaStore.Audio.Media.DATA);

            do {
                data.add( new ItemData(cursor.getString(songTitle),
                        cursor.getString(songArtist),
                        cursor.getString(songUrl)
                ));
            }
            while (cursor.moveToNext() && !cursor.isAfterLast());
        }
        adapter = new CustomAdapter(data);
        listView.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == MY_PERMISSION_REQUEST)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                getMusic();
            }
            else
            {
                Toast.makeText(this, "No Perrmission", Toast.LENGTH_SHORT).show();
                finish();
            }

        }

    }

}
