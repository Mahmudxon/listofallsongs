package uz.mahmudxon.listofallsongs.Models;

import android.graphics.Bitmap;

public class ItemData {
    String title;
    String Artist;
    String url;
    Bitmap img;

    public ItemData(String title, String artist, String url, Bitmap img) {
        this.title = title;
        Artist = artist;
        this.url = url;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return Artist;
    }

    public void setArtist(String artist) {
        Artist = artist;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }
}