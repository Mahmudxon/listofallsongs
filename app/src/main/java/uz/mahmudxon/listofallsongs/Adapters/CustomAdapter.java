package uz.mahmudxon.listofallsongs.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import uz.mahmudxon.listofallsongs.Models.ItemData;
import uz.mahmudxon.listofallsongs.R;

public class CustomAdapter extends BaseAdapter {
    ArrayList<ItemData> data;
    TextView title;
    TextView artist;
    ImageView img;
    public CustomAdapter(ArrayList<ItemData> data) {
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public ItemData getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        title = convertView.findViewById(R.id.text1);
        artist = convertView.findViewById(R.id.text2);
        img = convertView.findViewById(R.id.img_album);
        title.setText(data.get(position).getTitle());
        artist.setText(data.get(position).getArtist());

        return convertView;
    }
}
